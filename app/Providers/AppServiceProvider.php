<?php

namespace App\Providers;

use App\Auth\FusionAuth\UserProvider;
use FusionAuth\FusionAuthClient;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $key = env('FUSIONAUTH_API_KEY');
        $url = env('FUSIONAUTH_BASE_URL');
        $this->app->bind(
            FusionAuthClient::class,
            fn() => new FusionAuthClient($key, $url)
        );
        Auth::provider(
            'fusionauth',
            fn (Application $app) => $app->make(UserProvider::class)
        );
    }
}
