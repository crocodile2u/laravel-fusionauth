<?php

namespace App\Auth\FusionAuth;
use FusionAuth\FusionAuthClient;

class UserService
{
    private FusionAuthClient $authClient;

    /**
     * @param FusionAuthClient $authClient
     */
    public function __construct(FusionAuthClient $authClient)
    {
        $this->authClient = $authClient;
    }

    public function create(User $user, string $password): User
    {
        $clientRequest = [
            'registration' => ['applicationId' => env('FUSIONAUTH_APP_ID')],
            'sendSetPasswordEmail' => false,
            'user' => [
                'password' => $password,
                'fullName' => $user->fullName,
                'email' => $user->email,
                'passwordChangeRequired' => false,
                'twoFactorEnabled' => false,
            ],
        ];
        $clientResponse = $this->authClient->register(null, $clientRequest);
        if (!$clientResponse->wasSuccessful()) {
            // fusionauth provides all the details about an error that occured
            // but we are leaving this aside and an exercise for the reader
            throw new \Exception("cannot create user");
        }

        $userData = (array) $clientResponse->successResponse->user;
        return new User($userData);
    }

    public function retrieveById($identifier): ?User
    {
        $clientResponse = $this->authClient->retrieveUser($identifier);
        if (!$clientResponse->wasSuccessful()) {
            return null;
        }
        $userData = (array) $clientResponse->successResponse->user;
        return new User($userData);
    }

    public function retrieveByEmail($email): ?User
    {
        $clientResponse = $this->authClient->retrieveUserByEmail($email);
        if (!$clientResponse->wasSuccessful()) {
            return null;
        }
        $userData = (array) $clientResponse->successResponse->user;
        return new User($userData);
    }

    public function validateCredentials(array $credentials)
    {
        $clientRequest = [
            'applicationId' => env('FUSIONAUTH_APP_ID'),
            'loginId' => $credentials['email'],
            'password' => $credentials['password'],
        ];

        return $this->authClient->login($clientRequest)->wasSuccessful();
    }
}
