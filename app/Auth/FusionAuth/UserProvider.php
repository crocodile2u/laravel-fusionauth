<?php

namespace App\Auth\FusionAuth;

use Illuminate\Contracts\Auth\Authenticatable;

class UserProvider implements \Illuminate\Contracts\Auth\UserProvider
{
    private UserService $userService;

    /**
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function retrieveById($identifier): User
    {
        return $this->userService->retrieveById($identifier);
    }

    public function retrieveByToken($identifier, $token)
    {
        return null;
    }

    public function updateRememberToken(Authenticatable $user, $token)
    {
        return null;
    }

    public function retrieveByCredentials(array $credentials)
    {
        if (!$this->userService->validateCredentials($credentials)) {
            return null;
        }

        return $this->userService->retrieveByEmail($credentials["email"]);
    }

    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return $this->userService->validateCredentials($credentials);
    }
}
