<?php

namespace App\Auth\FusionAuth;

use Illuminate\Contracts\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\CanResetPassword;

class User implements Authorizable, Authenticatable, CanResetPassword, \JsonSerializable
{
    public readonly string|null $id;
    public readonly string|null $fullName;
    public readonly string|null $email;

    /**
     * @param array $data
     * @param string $fullName
     * @param string $email
     */
    public function __construct(array $data = [])
    {
        $this->id = $data['id'] ?? null;
        $this->fullName = $data['fullName'] ?? null;
        $this->email = $data['email'] ?? null;
    }

    public function getAuthIdentifierName()
    {
        return null;
    }

    public function getAuthIdentifier()
    {
        return $this->id;
    }

    public function getAuthPassword()
    {
        return null;
    }

    public function getRememberToken()
    {
        return null;
    }

    public function setRememberToken($value)
    {
        return null;
    }

    public function getRememberTokenName()
    {
        return null;
    }

    public function can($abilities, $arguments = [])
    {
        return false;
    }

    public function getEmailForPasswordReset()
    {
        return null;
    }

    public function sendPasswordResetNotification($token)
    {
        return null;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'name' => $this->fullName,
        ];
    }
}
