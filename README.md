## Laravel FusionAuth

You should have Composer (the PHP package manager), NodeJS, Docker and Docker Compose to run this project.
Once you have those installed:

```shell
composer install
npm install
docker-compose up
```

You should be good to go, open http://localhost:8000/ to see the result!
