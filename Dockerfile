# we start off from a CLI PHP image based on Alpine Linux distro
FROM php:8.2-cli-alpine AS php_base

# Create directory where our project will reside _inside_ the container
RUN mkdir /project
# Tell docker to cd to that directory
WORKDIR /project

# If our PHP installation needed any extra PHP extensions not available
# in the default docker image, we would add those here

# For the sake of simplicity, and not to dig into Docker setup too deep,
# I am only providing a minimal setup here, suitable for development.
# For production, you would probably want to start from a PHP-FPM base image,
# or even make a Nginx Unit image which can embed PHP module.

FROM php_base AS dev

# In dev mode, we are going to mount project directory as volume,
# inside the container
VOLUME /project
# ... and we're telling here that this image is going to run a server
# that listens on port 8000
EXPOSE 8000

# For development, I always install XDebug, this just helps A LOT, always
# here, I also provide a few settings for XDebug which should
# work with any recent Docker version and make the debug client able
# to connect to your editor/IDE of choice.
# The main trick here is using "host.docker.internal" as xdebug.client_host.
# Up until recently, this DNS entry was not available in Docker under Linux,
# but now it seems like we're good to use it. This host name should resolve
# to the docker daemon host machine address.
RUN apk add --virtual deps $PHPIZE_DEPS linux-headers && \
    pecl install xdebug && \
    echo "xdebug.client_host=host.docker.internal" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    echo "xdebug.mode=develop,debug" >> /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini && \
    docker-php-ext-enable xdebug && \
    apk del deps

# and this is the command we want to run in our container
# we listen on port 8000 (see the EXPOSE directive above),
# and we listen on all interfaces. If we skiped the --host switch,
# artisan would only listen to connections from localhost.
# REMEMBER: inside container localhost===[container host],
# and we will be connecting to this server from the host machine!
CMD php artisan serve --port=8000 --host=0.0.0.0
